/* compresses test files to CTX files (Softdisk compressed text)
 *
 * based on description of http://fileformats.archiveteam.org/wiki/Softdisk_Text_Compressor,
 * but there are errors in the description:
 * 1. the magic bytes are "0x034354303031" and not "0x034354303131"
 * 2. the table of fixed character sequence has 30 entries instead 29
 *
 * (c) 2020 by Andreas Romeyke, licensed under GPL 3.0 or later, see file COPYING for details
 */

use std::env;
use std::io::Seek;
use std::io::SeekFrom;
use std::io::Read;
use std::io::{self, Write};

const HEADER: [u8;6] = [
    0x03, 0x43, 0x54, 0x30, 0x30, 0x31
];
const TABLE_FIVECHAR_WORDS: [&str;30] = [
" the ",
" and ",
"SDPC",
" the",
" or ",
" an ",
" to ",
" of ",
" is ",
" it ",
" in ",
" on ",
"ing ",
" th",
"nd ",
" yo",
".  ",
"ed ",
" a ",
"er ",
"is ",
"or ",
"ng ",
"re ",
" pr",
"our",
"ou ",
" co",
"ion",
"ve "
];

const TABLE_BIGRAMS : [[u8;2];127] = [
    [0x65, 0x20],
    [0x20, 0x74],
    [0x74, 0x68],
    [0x73, 0x20],
    [0x20, 0x61],
    [0x68, 0x65],
    [0x74, 0x20],
    [0x69, 0x6e],
    [0x6e, 0x20],
    [0x64, 0x20],
    [0x65, 0x72],
    [0x72, 0x20],
    [0x20, 0x6f],
    [0x61, 0x6e],
    [0x72, 0x65],
    [0x6f, 0x75],
    [0x6f, 0x6e],
    [0x20, 0x69],
    [0x20, 0x73],
    [0x6f, 0x72],
    [0x79, 0x20],
    [0x20, 0x77],
    [0x74, 0x6f],
    [0x6f, 0x20],
    [0x2c, 0x20],
    [0x61, 0x72],
    [0x61, 0x74],
    [0x69, 0x73],
    [0x65, 0x73],
    [0x2e, 0x20],
    [0x6e, 0x64],
    [0x20, 0x63],
    [0x65, 0x6e],
    [0x74, 0x65],
    [0x20, 0x70],
    [0x20, 0x66],
    [0x76, 0x65],
    [0x68, 0x61],
    [0x6c, 0x65],
    [0x6e, 0x74],
    [0x73, 0x74],
    [0x69, 0x74],
    [0x72, 0x6f],
    [0x73, 0x65],
    [0x20, 0x6d],
    [0x66, 0x20],
    [0x20, 0x62],
    [0x6e, 0x67],
    [0x20, 0x79],
    [0x61, 0x6c],
    [0x65, 0x64],
    [0x6c, 0x6c],
    [0x74, 0x69],
    [0x72, 0x61],
    [0x79, 0x6f],
    [0x6f, 0x66],
    [0x6c, 0x20],
    [0x6d, 0x65],
    [0x75, 0x72],
    [0x65, 0x61],
    [0x20, 0x68],
    [0x20, 0x64],
    [0x61, 0x73],
    [0x61, 0x20],
    [0x68, 0x69],
    [0x63, 0x6f],
    [0x67, 0x20],
    [0x6e, 0x65],
    [0x6f, 0x6d],
    [0x64, 0x65],
    [0x61, 0x6d],
    [0x6c, 0x69],
    [0x70, 0x72],
    [0x20, 0x49],
    [0x69, 0x6c],
    [0x20, 0x6c],
    [0x75, 0x73],
    [0x68, 0x20],
    [0x72, 0x69],
    [0x75, 0x20],
    [0x63, 0x61],
    [0x20, 0x65],
    [0x75, 0x74],
    [0x69, 0x63],
    [0x65, 0x6c],
    [0x6f, 0x77],
    [0x20, 0x72],
    [0x6d, 0x20],
    [0x20, 0x67],
    [0x77, 0x69],
    [0x67, 0x72],
    [0x6c, 0x79],
    [0x65, 0x63],
    [0x65, 0x74],
    [0x62, 0x65],
    [0x66, 0x6f],
    [0x69, 0x6f],
    [0x6d, 0x61],
    [0x63, 0x65],
    [0x20, 0x75],
    [0x72, 0x73],
    [0x74, 0x61],
    [0x6c, 0x6f],
    [0x6c, 0x61],
    [0x20, 0x6e],
    [0x64, 0x69],
    [0x65, 0x65],
    [0x67, 0x65],
    [0x20, 0x54],
    [0x61, 0x76],
    [0x63, 0x74],
    [0x73, 0x69],
    [0x73, 0x73],
    [0x63, 0x68],
    [0x61, 0x63],
    [0x6e, 0x73],
    [0x75, 0x6e],
    [0x6d, 0x6f],
    [0x73, 0x75],
    [0x6f, 0x67],
    [0x73, 0x6f],
    [0x64, 0x6f],
    [0x6e, 0x6f],
    [0x20, 0x57],
    [0x68, 0x6f],
    [0x49, 0x20],
    [0x72, 0x74]
    ];

fn write_header () {
    io::stdout().write_all(&HEADER);
}

fn write_filename (infilename: String) {
    io::stdout().write_all(infilename.as_bytes());
    io::stdout().write( &[0u8]);
}

fn write_table_fivechar () {
    for entry in TABLE_FIVECHAR_WORDS.iter() {
        io::stdout().write_all(entry.as_bytes());
        if entry.as_bytes().len() < 5 {
            io::stdout().write( &[0u8]);
        }
    }
}

fn write_table_bigrams () {
    for entry in TABLE_BIGRAMS.iter() {
        io::stdout().write_all( entry);
    }
}

fn lookahead_fivechars (slice: &[u8]) -> std::option::Option<usize> {
    return TABLE_FIVECHAR_WORDS.iter().position(|&x| x.as_bytes() == slice) ;
}

fn check_and_write_fivechars (slice : &[u8], readbytes : &mut usize ) -> bool {
    /* lookahead fivechars */
    let lookahead = lookahead_fivechars(slice);
    let res = match lookahead {
        None => false,
        Some (n)  => {
            let out: u8 = match n {
                0..=9 => n as u8 ,
                10..=11 => n as u8 + 1,
                _ => n as u8 + 2,
            };
            io::stdout().write(&[out]);
            let bytes = TABLE_FIVECHAR_WORDS[n].as_bytes().len();
            *readbytes += bytes;
            eprintln!("check_and_write_fivechars, readbytes={}", *readbytes);
            true
        }
    };
    res
}

fn lookahead_bigrams (slice: &[u8]) -> std::option::Option<usize> {
    return TABLE_BIGRAMS.iter().position(|&x| x.len() == slice.len() && x[0] == slice[0] && x[1]==slice[1]) ;
}

fn check_and_write_bigrams (slice: &[u8], readbytes : &mut usize ) -> bool {
    /* lookahead bigrams */
    let lookahead = lookahead_bigrams(slice);
    let res= match lookahead {
        None => false,
        Some (n) => {
            io::stdout().write(&[128+n as u8]);
            *readbytes +=2;
            eprintln!("check_and_write_bigrams, readbytes={}", *readbytes);
            true
        }
    };
    return res;
}

fn lookahead_rle (slice: &[u8]) -> std::option::Option<usize> {
    let first_val = slice[0];
    let mut index = 0;
    for i in 1.. slice.len() {
        if slice[i] != first_val {
            index = i;
            break;
        }
    }
    if index > 1 { return Some(index); }
    else {return None;}
}

fn check_and_write_rle(slice: &[u8], readbytes : &mut usize ) -> bool  {
    let lookahead = lookahead_rle(slice);
    let res= match lookahead {
        None => false,
        Some (n) => {
            io::stdout().write(&[255]);
            io::stdout().write(&[30+n as u8]);
            io::stdout().write(&[slice[0]]);
            *readbytes += n as usize;
            eprintln!("check_and_write_rle, readbytes={}", *readbytes);
            true
        }
    };
    return res;
}

fn store_printable (ch: u8) -> usize {
    io::stdout().write(&[ch]);
    1
}

fn store_escaped_nonprintable (ch : u8) -> usize {
    io::stdout().write(&[255]);
    io::stdout().write(&[ch]);
    1
}

fn write_compressed_data(mut file: std::fs::File) {
    loop {
        const BUFSIZE: usize = 8192;
        let mut buffer = [0u8; BUFSIZE]; // 8kb buffer
        let pos = file.read(&mut buffer);
        if pos.is_err() {
            eprintln!("Could not read stream");
            std::process::exit(-1);
        }
        let read_position = pos.unwrap();
        if read_position == 0 { break; } // EOF reached
        let mut already_read: usize = 0;
        while (already_read < (BUFSIZE - 256)) && (
            already_read < read_position
        ) {
            /* already read in pos chars in buffers */
            let ch = buffer[already_read];
            if ch == 0xa {
                already_read = already_read + 1;
                eprintln!("newline, readbytes={}", already_read);
            } else {
                /* lookahead fivechars */
                if !check_and_write_fivechars(&buffer[already_read..already_read + 5], &mut already_read) {
                    /* lookahead bigrams */
                    if !check_and_write_bigrams(&buffer[already_read..already_read + 2], &mut already_read) {
                        /* lookahead rle */
                        if !check_and_write_rle(&buffer[already_read..already_read + 127], &mut already_read) {
                            if (ch >= 32 && ch <= 127) || ch == 0xd {
                                already_read = already_read + store_printable(ch);
                                eprintln!("printable, readbytes={}", already_read);
                            } else {
                                already_read = already_read + store_escaped_nonprintable(ch);
                                eprintln!("nonprintable, readbytes={}", already_read);
                            }
                        }
                    }
                }
            }
        }
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let infile_name = &args[1];
    eprintln!("ctx_compress, (c) by Andreas Romeyke, licensed under GPL3.0");
    eprintln!("encoding file '{}'", infile_name);
    let file = std::fs::File::open(infile_name).unwrap();
    write_header();
    write_filename(infile_name.to_string());
    write_table_fivechar();
    write_table_bigrams();
    write_compressed_data(file);
}
