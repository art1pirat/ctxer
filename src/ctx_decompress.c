/* decompresses CTX files (Softdisk compressed text)
 *
 * based on description of http://fileformats.archiveteam.org/wiki/Softdisk_Text_Compressor,
 * but there are errors in the description:
 * 1. the magic bytes are "0x034354303031" and not "0x034354303131"
 * 2. the table of fixed character sequence has 30 entries instead 29
 *
 * (c) 2019 by Andreas Romeyke, licensed under GPL 3.0 or later, see file COPYING for details
 */

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

int parse_header(FILE * filehandle ) {
    if (fseek( filehandle, 0L, SEEK_SET) != 0L) {
        perror("Could not seek to 0L");
        exit(EXIT_FAILURE);
    }
    u_int8_t magic_bytes[6];
    if (fread(&magic_bytes, 1, 6, filehandle ) != 6) {
        perror("Could not read first 6 bytes for header");
        exit(EXIT_FAILURE);
    }
    if (
        (magic_bytes[0] == 0x03) &&
        (magic_bytes[1] == 0x43) &&
        (magic_bytes[2] == 0x54) &&
        (magic_bytes[3] == 0x30) &&
        (magic_bytes[4] == 0x30) &&
        (magic_bytes[5] == 0x31)
    ) {
        return 0;
    }
    fprintf( stderr, "File not a CTX-file, found this header instead:");
    for (int i=0; i<6; i++) {
        fprintf( stderr, " %0x", magic_bytes[i]);
    }
    fprintf(stderr, "\n");
    return 1;
}

char * parse_filename(FILE * filehandle) {
    static char filename[14];
    int idx = 0;
    while (!feof(filehandle)) {
        if (idx > 13) break;
        char ch = fgetc( filehandle);
        if (ch == 0 ) break;
        filename[idx++] = ch;
    }
    return filename;
}

void parse_fixed_character_sequence(FILE * filehandle, char table[31][6]) {
    //fseek( filehandle, 1, SEEK_CUR);
    for (int i=0; i<31; i++) {
        for (int j = 0; j < 5; j++) {
            table[i][j] = 0;
        }
    }
    for (int i=0; i<30; i++) {
        for (int j=0; j<5; j++) {
            int ch = fgetc( filehandle);
            if (!feof(filehandle)) {
                if (ch == 0) {
                    break;
                } else {
                    assert(i < 31);
                    assert(j < 6);
                    table[i][j] = (unsigned char) ch;
                }
            }
        }
    }
}

void parse_fixed_twocharacter_sequence(FILE * filehandle, char table[127][2]) {
    for (int i=0; i<127; i++) {
        for (int j=0; j<2; j++) {
            if ( !feof(filehandle)) {
                int ch = fgetc( filehandle);
                table[i][j] = ch;
            }
        }
    }
}



int main (const int argc, const char* argv[]) {
    const char *infilename = NULL;
    if (argc != 2) {
        fprintf(stderr, "expecting 1 argument, got %i\n", argc);
        exit(EXIT_FAILURE);
    }
    infilename = argv[1];
    fprintf(stderr, "ctx_decompress, (c) by Andreas Romeyke, licensed under GPL3.0\n");
    fprintf(stderr, "decoding file '%s':\n", infilename);
    FILE * filehandle;
    filehandle = fopen( infilename, "rb");
    parse_header(filehandle);
    fprintf(stderr, "\toriginal filename: '%s'\n", parse_filename( filehandle));
    char table[31][6];
    char table2[127][2];
    parse_fixed_character_sequence(filehandle, table);
#ifdef DEBUG
    for (int i=0; i<31; i++) {
        fprintf(stderr, "%i = '%s'\n", i, table[i]);
    }
#endif
    parse_fixed_twocharacter_sequence(filehandle, table2);
#ifdef DEBUG
    for (int i=0; i<127; i++) {
        fprintf(stderr, "%i = '%c','%c'\n", i, table2[i][0], table2[i][1]);
    }
#endif
    // decompress the rest
    while( !feof( filehandle ) ) {
        int ch = fgetc( filehandle);
        if (feof(filehandle)) {
            exit(EXIT_SUCCESS);
        }
#ifdef DEBUG
        if (ch < 0 || ch > 255) {
            fprintf(stderr, "ASSERT, ch=%i out of range!\n");
            exit(EXIT_FAILURE);
        }
#endif
        if ((ch >= 0) && (ch <= 9)) {
            printf("%s", table[ch]);
        } else if ((ch >= 11) && (ch <= 12)) {
            printf("%s", table[ch - 1]);
        } else if (ch == 13) {
            printf("%c%c", '\r', '\n');
        } else if ((ch >= 14) && (ch <= 31)) {
            printf("%s", table[ch-2]);
        } else if ((ch >= 128) && (ch <= 254)) {
            printf("%c", table2[ch-128][0]);
            printf("%c", table2[ch-128][1]);
        } else if (ch == 255) {
            int nextch = fgetc( filehandle );
#ifdef DEBUG
            if (nextch < 0 || nextch > 255) {
                fprintf(stderr, "ASSERT, nextch %i out of range!\n", nextch);
                exit(EXIT_FAILURE);
            }
#endif
            if (feof(filehandle)) {
                exit(EXIT_SUCCESS);
            }
            int repeat = 0;
            if ((nextch >=32) && (nextch <= 127)) {
                repeat = nextch - 30;
            }
            int nextnextch = fgetc( filehandle );
            if (feof(filehandle)) {
                exit(EXIT_SUCCESS);
            }
            for (int i =0; i< repeat; i++) {
                printf ("%c", nextnextch);
            }
        } else {
            printf ("%c", ch);
        }

    }
    exit(EXIT_SUCCESS);
}
