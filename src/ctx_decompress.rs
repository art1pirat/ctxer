/* decompresses CTX files (Softdisk compressed text)
*
* based on description of http://fileformats.archiveteam.org/wiki/Softdisk_Text_Compressor,
* but there are errors in the description:
* 1. the magic bytes are "0x034354303031" and not "0x034354303131"
* 2. the table of fixed character sequence has 30 entries instead 29
*
* to compile type: rustc ctx_decompress.rs
*
* (c) 2020 by Andreas Romeyke, licensed under GPL 3.0 or later, see file COPYING for details
*/
use std::env;
use std::io::Seek;
use std::io::SeekFrom;
use std::io::Read;

fn parse_header(mut file: & std::fs::File) {
    let pos = file.seek(SeekFrom::Start(0));
    if pos.is_err()
    {
        eprintln!("Could not seek to 0L");
        std::process::exit( -1 );
    }
    let mut magic_bytes = [0u8; 6];
    let pos = file.read(&mut magic_bytes);
    if (pos.is_err()) || (pos.unwrap() == 0) {
        eprintln!("Could not read header");
        std::process::exit( -1 );
    }
    if
        (magic_bytes[0] != 0x03) ||
            (magic_bytes[1] != 0x43) ||
            (magic_bytes[2] != 0x54) ||
            (magic_bytes[3] != 0x30) ||
            (magic_bytes[4] != 0x30) ||
            (magic_bytes[5] != 0x31)
    {
        eprintln!("File not a CTX-file, found this header instead:");
        for i in 0 .. 6 {
            eprint!("{0:#x} ", magic_bytes[i]);
        }
        eprintln!();
        std::process::exit( -1 );
    }
}
fn parse_filename(mut file: &std::fs::File) -> String {
    let mut buffer = [0u8;1];
    let mut filename = String::new();
    loop {
        let pos = file.read(&mut buffer);
        if pos.is_err() || pos.unwrap() == 0 {
            eprintln!("Could not read original filename");
            std::process::exit(-1);
        }
        if buffer[0] == 0 {
            break;
        } else {
            filename.push(buffer[0] as char);
        }
    }
    return filename;
}
fn parse_fixed_character_sequence(mut file: &std::fs::File) -> [[u8;5];30] {
    let mut table = [[0u8;5];30];
    for i in 0 .. 30 {

        let mut buffer =[0u8; 1];
        for j in 0 .. 5 {
            let pos = file.read(&mut buffer);
            if pos.is_err() || pos.unwrap() == 0 {
                eprintln!("Could not read fixed character sequence");
                std::process::exit(-1);
            }
            if buffer[0] == 0 {
                break;
            } else {
                table[i][j] = buffer[0];
            }
        }
    }
    let ret_table = table;
    return ret_table;
}

fn parse_fixed_twocharacter_sequence( mut file: &std::fs::File) -> [[u8;2];127] {
    let mut table = [[0u8;2];127];
    let mut buffer =[0u8; 1];
    for i in 0 .. 127 {
        for j in 0 .. 2 {
            let pos = file.read(&mut buffer);
            if pos.is_err() || pos.unwrap() == 0 {
                eprintln!("Could not read twocharacter sequence");
                std::process::exit(-1);
            }
            table[i][j] = buffer[0];
        }
    }
    let ret_table = table;
    return ret_table;
}

fn rle_print (ch: char, repeat: u8) {
    for _i in 0 .. repeat {
        print!( "{0}", ch);
    }
}

fn decode_repeat (mut file: &std::fs::File) {
    let mut buffer = [0u8; 1];
    let pos = file.read(&mut buffer);
    if pos.is_err() || pos.unwrap() == 0 {
        eprintln!("Could not read compressed stream (decode_repeat(), get repeat)");
        std::process::exit(-1);
    }
    let ch = buffer[0];
    // eprintln!("decode_repeat, repeat={}", ch);
    let repeat = match ch {
        32..=127 => ch - 30,
        _ => 0
    };
    let pos = file.read(&mut buffer);
    if pos.is_err() || pos.unwrap() == 0 {
        eprintln!("Could not read compressed stream (decode_repeat(), get char)");
        std::process::exit(-1);
    }
    // eprintln!("decode_repeat, char={}", buffer[0]);
    rle_print( buffer[0] as char, repeat);
}

fn table_print (table: &[[u8;5];30] , i: usize) {
    assert!( i < 30);
    for j in 0 .. 5 {
        if 0 == table[i][j] {
            break;
        }
        print!("{0}", table[i][j] as char)
    }
}

fn table2_print (table2: &[[u8;2];127], i: usize) {
    assert!(i<127);
    print! ("{0}{1}", table2[i][0] as char, table2[i][1] as char)
}

fn decompress (mut file: &std::fs::File, table: &[[u8;5];30], table2: &[[u8;2];127]) {
    let mut buffer =[0u8; 1];
    loop {
        let pos = file.read(&mut buffer);
        if pos.is_err() {
            eprintln!("Could not read compressed stream (decompress(), get char)");
            std::process::exit(-1);
        }
        if pos.unwrap() == 0 { break; } // EOF reached
        let ch = buffer[0];
        let i = ch as usize;
        match ch {
            0 ..=9  => table_print( &table, i),
            11|12  => table_print( &table, i-1),
            13 => print!("\r\n"),
            14 ..=31 => table_print( &table, i-2),
            128 ..=254 => table2_print(&table2, i-128),
            255 => decode_repeat( &file),
            _ => print!("{0}", ch as char)
        }
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let infile_name = &args[1];
    eprintln!("ctx_decompress, (c) by Andreas Romeyke, licensed under GPL3.0" );
    eprintln!("decoding file '{}'", infile_name);
    let file = std::fs::File::open(infile_name).unwrap();
    parse_header(&file);
    let original_filename = parse_filename(&file);
    eprintln!("original filename '{}'", original_filename);
    let table = parse_fixed_character_sequence(&file);
    let table2 = parse_fixed_twocharacter_sequence(&file);
    decompress(&file, &table, &table2);
}